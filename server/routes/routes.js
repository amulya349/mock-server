var express = require('express');
var passport = require('passport');
var User = require('../models/user');
var Listing = require('../models/listing');
var Occasion = require('../models/occasion');
var Comment = require('../models/comment');
var router = express.Router();
var multer = require('multer');
var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var rimraf = require('rimraf');

var profilePicUpload = multer({
  dest: __dirname + '/../../resources/public/uploads/profilepics',
})

var listingPicUpload = multer({
  dest: __dirname + '/../../resources/public/uploads/listings',
})

var occasionUpload = multer({
    dest: __dirname + '/../../resources/public/uploads/occasions',
})

router.get('/', function(req, res){
    res.status(200).send("The server is running. Thanks for stopping by... :)");
});

router.post('/login', function(req, res, next) {
  passport.authenticate('local-login', function(err, user, info) {
    if (err) { 
        res.json({message: err});
        return;
    }
    if (!user) {    
        res.json(info);
        return; 
    }
    req.logIn(user, function(err) {
        if (err) { return next(err); }
        //If successfull, below will run;
        var data = req.user;
        // data.__v = undefined;
        data.local.password = undefined;
        res.status(200).json(data);
    });
  })(req, res, next);
});

router.get('/logout', function(req, res) {
    req.logout();
    res.sendStatus(200);
});


router.post('/register', function(req, res) {
    User.findOne({ 'local.email' :  req.body.email }, function(err, user){
        if(err){
            res.json(err);
            return;
        }
        else if(user){
            res.json({message: "user already exists."})
        }
        else{
            var newUser = new User();
            newUser.local.email = req.body.email;
            newUser.local.password = newUser.generateHash(req.body.password);
            newUser.local.username = req.body.username;
            // newUser.local.fname = req.body.fname;
            // newUser.local.lname = req.body.lname;
            // newUser.local.city = req.body.city;
            // newUser.local.country = req.body.country;

            newUser.save(function(err){
                if(err){
                    console.log(err);
                    return;
                }
                var data = newUser;
                if(data.local.password)
                    data.local.password = undefined;
                // data.__v = undefined;
                if(data.created)
                    data.created = undefined;
                res.json(data);
	            mkdirp(path.join(__dirname,'..','..','resources','public','uploads','profilepics'), function (err, made) {
				    if (err) console.error(err)
				    else console.log('profilePic dir!: '+made);
				});
            })
        }
    })
});

router.get('/listusers', function(req, res){
	User.find({}, function(err, user){
		res.json(user);
	})
})

router.get('/profile/:userid', function(req, res){
    User.findById(req.params.userid, function(err, user){
        if(err){
            res.json(err);
            return;
        }
        if(!user){
            res.json({message: "No profile found with this userid !"});
            return;
        }
        var data = user;
        // data.__v = undefined;
        if(data.created)
            data.created = undefined;
        res.json(data);
    })
})

router.put('/profile', function(req, res){
    User.findById(req.body.userid, function(err, user){
        if(err){
            res.json(err);
            return;
        }
        if(!user){
            res.json({message: "No profile found with this userid !"});
            return;
        }
        var d = req.body;
        if(d.username)
            user.local.username = d.username;
        if(d.password)
            user.local.password = d.password;
        if(d.fname)
            user.local.fname = d.fname;
        if(d.lname)
            user.local.lname = d.lname;
        if(d.email)
            user.local.email = d.email;
        if(d.city)
            user.local.city = d.city;
        if(d.country)
            user.local.country = d.country;
        user.local.profilePicUrl = d.profilePicUrl;

        user.save(function(err){
            if(err) throw err;
            res.status(200).json({message: 'success'});
        })
    })   
})

router.get('/privacysettings/:userid', function(req, res){
    User.findById(req.params.userid, function(err, user){
        if(err){
            res.json(err);
            return;
        }
        res.json(user.local.privacySettings);
    })
})

router.put('/privacySettings', function(req, res){
    User.findById(req.body.userid, function(err, user){
        if(err){
            res.json(err);
            return;
        }
        var d = req.body;
        user.local.privacySettings.canSeeProfile = d.canSeeProfile;
        user.local.privacySettings.canSeeActivity = d.canSeeActivity;
        user.local.privacySettings.canSeeYourFriends = d.canSeeYourFriends;
        user.local.privacySettings.visible = d.visible;

        user.save(function(err){
            if(err) throw err;
            res.status(200).json({message: 'success'});
        })
    })
})

router.get('/listings', function(req, res){
    if(req.query.userid){
        Listing.findById(req.query.userid, function(err, data){
            res.json(data);
            
        })
    }else if(req.query.city){
    
	    Listing.find({'city': req.query.city}, function(err, data){
	        res.json(data);
	    })
	}
	else if(req.query.lat){
		Listing.find({ 'lat' : req.query.lat , 'lon': req.query.lon}, function(err, data){
	        res.json(data);
	    })
	}
    else if(req.query.occasionid){
        Listing.find({ 'occasionId' : req.query.occasionid}, function(err, data){
            res.json(data);
        })
    }
	else{
		Listing.find({}, function(err, data){
	        res.json(data);
	    })
	}
	
})

router.post('/listings', function(req, res){
    
    var data = new Listing();
    data.name = req.body.name;
    data.street = req.body.street;
    data.city = req.body.city;
    data.zipcode = req.body.zipcode;
    data.state = req.body.state;
    data.phone = req.body.phone;
    data.priceRate = req.body.priceRate;
    data.ratingStars = req.body.ratingStars;
    data.menuUrl = req.body.menuUrl;
    data.imageUrls = req.body.imageUrls;
    data.numberOfReviews = req.body.numberOfReviews; 
    // data.occasionId = req.body.occasionId;
    data.lon = req.body.lon;
    data.lat = req.body.lat;
    if(req.body.occasionId){
        var x = req.body.occasionId.split(' ');
        for(i=0; i<x.length; i++){
            if(x[i] != '')
                data.occasionId.push(x[i]);
        }    
    }
    // if(req.body.dietryInfo){
    // 	var x = req.body.dietryInfo;
	   //  for(i=0; i<x.length; i++){
	   //  	data.dietryInfo.push(x[i]);
	   //  }
    // }
	data.dietryInfo = req.body.dietryInfo;
    data.save(function(err){
        if(err){
            console.log(err);
            return;
        }
        res.json(data);
        mkdirp(path.join(__dirname,'..','..','resources','public','uploads','listings',(data._id).toString()), function (err, made) {
		    if (err) console.error(err)
		    else console.log('listing dir created:'+made)
		});
    })

})

router.post('/upload', profilePicUpload.single('image'), function(req, res, next){
    
    fs.rename(req.file.path, __dirname + '/../../resources/public/uploads/profilepics/'+req.body.userid+'.jpg', function(err){
        if(err)
            console.log(err);
    })
    User.findById(req.body.userid, function(err, user){
        if(err){
            res.json(err);
            return;
        }
        user.local.profilePicUrl = '/uploads/profilepics/'+req.body.userid+'.jpg';

        user.save(function(err){
            if(err) throw err;
            res.status(200).json({status: 'success', url: user.local.profilePicUrl});
        })
    })
})

router.get('/upload', function(req, res){
    res.send('<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>upload</title></head><body><form action="/upload" method="POST" enctype="multipart/form-data">Select an image to upload:<input type="file" name="image"><input type="text" name="userid"><input type="submit" value="Upload Image"></form></body></html>')
})

router.post('/listings/upload', listingPicUpload.single('image'), function(req, res, next){
	var imgUrl = '/uploads/listings/'+req.body.userid+'/'+Date.now().toString()+'.jpg';
	fs.rename(req.file.path, __dirname + '/../../resources/public/uploads/listings/'+req.body.userid+'/'+Date.now().toString()+'.jpg', function(err){
        if(err)
            console.log(err);
    })
    
    Listing.findByIdAndUpdate(req.body.userid,
    	{ $push: {"imageUrls": imgUrl}},
     	{ safe: true, upsert: true}, 
     	function(err, data){
	        if(err){
	            res.json(err);
	            return;
	        }
	        res.json(data);
        })
})

router.get('/listings/upload', function(req, res){
    res.send('<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>upload</title></head><body><form action="/listings/upload" method="POST" enctype="multipart/form-data">Select an image to upload:<input type="file" name="image"><input type="text" name="userid"><input type="submit" value="Upload Image"></form></body></html>')
})

//Delete methods
router.delete('/deletelisting/:userid', function(req, res){
    Listing.findByIdAndRemove(req.params.userid, function(err) {
        if (err) throw err;

        rimraf(path.join(__dirname,'..','..','resources','public','uploads','listings',req.params.userid), function(err) {
          if (err) { throw err; }
          // done
        });
        res.json({message: "Deleted listing successfully"})
    });
})

router.delete('/deleteuser/:userid', function(req, res){
    User.findById(req.params.userid, function(err, user) {
        if (err) throw err;
        var picPath = path.join(__dirname,'..','..','resources','public','uploads','profilepics',req.params.userid+'.jpg');
        fs.unlink(picPath, function(err){
            if(err) {
                // console.log(err);
            };
        })
        if(!user){
            res.json({message: "user not found"});
            return;
        }
        user.remove(function(err){
            if(err) throw err;
            res.json({message: "successfully removed user!"})
        })
        
    });
})

router.delete('/deleteallusers', function(req, res){
    User.remove({}, function(err) {
        if (err) throw err;

        rimraf(path.join(__dirname,'..','..','resources','public','uploads','profilepics'), function(err) {
          if (err) { throw err; }
            console.log("profilepics directory cleared")
        });

        res.json({message: "all users deleted"})
    });
})

router.delete('/deletealllistings', function(req, res){
    Listing.remove({}, function(err) {
        if (err) throw err;

        rimraf(path.join(__dirname,'..','..','resources','public','uploads','listings'), function(err) {
          if (err) { throw err; }
            console.log("listings directory cleared")
        });

        res.json({message: "all listings deleted"})
    });
})

router.get('/getoccasion', function(req, res){
    if(req.query.userid){
        Occsion.findById(req.query.userid, function(err, data){
            res.json(data);
        })
    }else if(req.query.city){
        Occasion.find({'city': req.query.city}, function(err, data){
            res.json(data);
        })
    }
    else{
        Occasion.find({}, function(err, data){
            res.json(data);
        })
    }
})

router.post('/addoccasion', function(req, res){
    var occ = new Occasion();
    occ.name = req.body.name;
    occ.city = req.body.city;
    occ.save(function(err){
        if(err){
            console.log(err);
            return;
        }
        res.json(occ);
    })
})

router.get('/occasion/upload', function(req, res){
    res.send('<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>upload</title></head><body><form action="/upload/occasion" method="POST" enctype="multipart/form-data">Select an image to upload:<input type="file" name="image"><input type="text" name="userid"><input type="submit" value="Upload Image"></form></body></html>')
})

router.post('/occasion/upload', occasionUpload.single('image'), function(req, res, next){

    fs.rename(req.file.path, __dirname + '/../../resources/public/uploads/occasions/'+req.body.userid+'.jpg', function(err){
        if(err)
            console.log(err);
    })
    Occasion.findById(req.body.userid, function(err, occ){
        if(err){
            res.json(err);
            return;
        }
        occ.imageUrl= '/uploads/occasions/'+req.body.userid+'.jpg';

        occ.save(function(err){
            if(err) throw err;
            res.status(200).json({status: 'success', url: occ.imageUrl});
        })
    })
})

router.delete('/deletealloccasions', function(req, res){
    Occasion.remove({}, function(err) {
        if (err) throw err;

        rimraf(path.join(__dirname,'..','..','resources','public','uploads','occasions'), function(err) {
          if (err) { throw err; }
            console.log("occasions directory cleared")
        });

        res.json({message: "all occasions deleted"})
    });
})

router.delete('/deleteoccasion/:userid', function(req, res){
    Occasion.findById(req.params.userid, function(err, occ) {
        if (err) throw err;
        var occPath = path.join(__dirname,'..','..','resources','public','uploads','occasions',req.params.userid+'.jpg');
        fs.unlink(occPath, function(err){
            if(err) {
                // console.log(err);
            };
        })
        if(!occ){
            res.json({message: "occasion not found"});
            return;
        }
        occ.remove(function(err){
            if(err) console.log(err);
            res.json({message: "successfully removed occasion!"})
        })
        
    });
})

router.put('/occasion/addlisting', function(req, res){
    Occasion.findById(req.body.userid, function(err, data){
        if(req.body.listingid){
            var x = req.body.listingid.split(' ');
            for(i=0; i<x.length; i++){
                data.listingId.push(x[i]);
            } 
        }
        data.save(function(err){
            res.json(data);
        })   
    })
})

router.post('/addcomment', function(req, res){
    Listing.findById(req.body.listingid, function(err, data){
        if(!data){
            res.json({message: "No listing found with this id !"});
            return;
        }
        var cmm = new Comment();
        cmm.userId = req.body.userid;
        cmm.rating = req.body.rating;
        cmm.listingId = req.body.listingid;
        if(req.body.data)
            cmm.data = req.body.data;
        cmm.save(function(err){
            if(err)
                console.log(err);
            res.json(cmm);
        }); 
    })
})

router.get('/getcomment', function(req, res){
    if(req.query.listingid)
    {
        Comment.find({listingId: req.query.listingid}, function(err, cmm){
            if (err) console.log(err);
            if(!cmm){
                res.json({message: "Comment not found"});
                return;
            } 
            res.json(cmm);
        })
    }
    else if(req.query.commentid){
        Comment.findById(req.query.commentid, function(err, cmm){
            if (err) console.log(err);
            if(!cmm){
                res.json({message: "Comment not found"});
                return;
            } 
            res.json(cmm);
        })
    }
    else{
        Comment.find({},function(err,cmm){
            if (err) console.log(err);
            if(!cmm){
                res.json({message: "Comment not found"});
                return;
            } 
            res.json(cmm);
        })
    }

})

router.delete('/deletecomment/:commentid', function(req, res){
    Comment.findById(req.params.commentid, function(err, cmm) {
        if (err) console.log(err);
        if(!cmm){
            res.json({message: "Comment not found"});
            return;
        }
        cmm.remove(function(err){
            if(err) console.log(err);
            res.json({message: "successfully removed comment!"})
        })
    });
})

router.delete('/deleteallcomments', function(req, res){
    Comment.remove({}, function(err) {
        if (err) throw err;
        res.json({message: "all comments deleted"})
    });
})

module.exports = router;
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var occasionSchema = mongoose.Schema({
    name : String, 
    city : String, 
    imageUrl: String,
    listingId: [{ type: Schema.Types.ObjectId, ref: 'Listing' }],
    created : String
});


occasionSchema.pre('save', function(next) {

  var currentTime = new Date().toString();
  this.created = currentTime;
  next();
});

module.exports = mongoose.model('Occasion', occasionSchema);

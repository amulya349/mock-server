var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({

    local : {
        username : { type: String, required: true, unique: true },
        password : { type: String, required: true },
        fname : String,
        lname : String,
        email : { type: String, required: true, unique: true },
        city : String, 
        country : String,
        profilePicUrl: String,
        notifyMessage : Boolean,
        notifyActivity : Boolean,
        
        privacySettings : {
            canSeeProfile : Number,
            canSeeActivity : Number, 
            canSeeYourFriends : Number,
            visible : Number
        }
    },
    created : String
});

userSchema.pre('save', function(next) {
  // get the current date
  var currentTime = new Date().toString();
  this.created = currentTime;
  next();
});

// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);

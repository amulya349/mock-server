var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var commentSchema = mongoose.Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'User' },
    rating: { type: Number, min: 1, max: 10 },
    listingId: { type: Schema.Types.ObjectId, ref: 'Listing' },
    data : String, 
    created : String
});


commentSchema.pre('save', function(next) {

  var currentTime = new Date().toString();
  this.created = currentTime;
  next();
});

module.exports = mongoose.model('Comment', commentSchema);

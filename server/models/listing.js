var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var listingSchema = mongoose.Schema({
    name : String,
    street : String, 
    city : String, 
    zipcode : String, 
    state : String, 
    phone : String,
    ratingStars : Number,
    priceRate : Number,
    menuUrl : String,
    imageUrls : [String],
    numberOfReviews : Number,
    dietryInfo : String,
    lon : String,
    lat : String,
    occasionId: [{ type: Schema.Types.ObjectId, ref: 'Occasion' }],  
    created : String
});


// methods ======================
listingSchema.pre('save', function(next) {
  // get the current date
  var currentTime = new Date().toString();
  this.created = currentTime;
  next();
});

module.exports = mongoose.model('Listing', listingSchema);

var LocalStrategy    = require('passport-local').Strategy;

var User       = require('../models/user');

module.exports = function(passport) {

    // passport session setup 
   
    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    
    // LOCAL LOGIN 
    passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {
        process.nextTick(function() {
            User.findOne( {$or:[{'local.email' :  email},{'local.username' : email}] }, function(err, user) {
                if (err)
                    return done(err);

                // if no user is found, return the message
                if (!user)
                    return done(null, false, {message: "No user found"});

                if (!user.validPassword(password))
                    return done(null, false, {message: 'Oops! Wrong password.'});

                // all is well, return user
                else
                    return done(null, user);
            });
        });

    }));

    // LOCAL SIGNUP 
    passport.use('local-signup', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req, email, password, done) {
        console.log("Local Strategy");
        if (email)
            email = email.toLowerCase(); 

        process.nextTick(function() {
            // if the user is not already logged in:
            if (!req.user) {
                User.findOne({ 'local.email' :  email }, function(err, user) {
                    // if there are any errors, return the error
                    if (err)
                        return done(err, false);

                    // check to see if there's already a user with that email
                    if (user) {
                        return done(null, false );
                    } else {

                        // create the user
                        var newUser = new User();

                        newUser.local.email    = email;
                        newUser.local.password = newUser.generateHash(password);
                        newUser.local.fname = req.body.fname;
                        newUser.local.lname = req.body.lname;
                        newUser.local.city = req.body.city;
                        newUser.local.country = req.body.country;
                        newUser.local.username = req.body.username;
                        newUser.save(function(err) {
                            if (err){
                                console.log("Error in local-signup db.save()\n"); 
                                return done(err);
                            }
                            console.log("New Local User Created !\n");
                            return done(null, newUser);
                        });
                    }

                });
            } else {
                return done(null, req.user);
            }

        });

    }));

};
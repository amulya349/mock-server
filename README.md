# mock-server

This is a server giving services via REST API. 

### User Schema: 
```js
    {
      _id : String,
      local : {
        username : { type: String, required: true, unique: true },
        password : { type: String, required: true },
        fname : String,
        lname : String,
        email : { type: String, required: true, unique: true },
        city : String, 
        country : String,
        profilePicUrl: String,
        notifyMessage : Boolean,
        notifyActivity : Boolean,
        
        privacySettings : {
            canSeeProfile : Number,
            canSeeActivity : Number, 
            canSeeYourFriends : Number,
            visible : Number
        }
    },
    created : String
}
```
### Listing Schema:
```js
{
    _id : String,
    name : String,
    street : String, 
    city : String, 
    zipcode : String, 
    state : String, 
    phone : String,
    ratingStars : Number,
    priceRate : Number,
    menuUrl : String,
    imageUrls : [String],
    numberOfReviews : Number,
    dietryInfo : String,
    lon : String,
    lat : String,
    occasionId: [{ type: Schema.Types.ObjectId, ref: 'Occasion' }],
    created : String
}
```

### Occasion Schema
```js
{
    name : String, 
    city : String, 
    imageUrl: String,
    listingId: [{ type: Schema.Types.ObjectId, ref: 'Listing' }],
    created : String
}
```

### Comment Schema
```js
{
    userId: { type: Schema.Types.ObjectId, ref: 'User' },
    rating: { type: Number, min: 1, max: 10 },
    listingId: { type: Schema.Types.ObjectId, ref: 'Listing' },
    data : String, 
    created : String
}
```
### URL routes: 
>NOTE: All POST methods are `application/x-www-form-urlencoded` except `/upload` which is `multipart/form-data`.
- `/login`    [POST]
   - input: `email` (email/username), `password`
   - output: all user details except password
- `/register`    [POST]
   - input: `email`, `password`, `username`
   - output: all user details except password
- `/listusers`    [GET]
   - output: details of all users (only for debugging purposes)
- `/profile/<userid>`    [GET]
   - input: `userid`
   - output: all user details
- `/profile`    [PUT]
   - input: `userid`
   - output: `{message: 'success'}`
- `/privacysettings/<userid>`    [GET]
   - input: `userid`
   - output: `canSeeProfile`, `canSeeActivity`, `canSeeYourFriends`, `visible`
- `/privacysettings/<userid>`    [PUT]
   - input: `userid`, `canSeeProfile`, `canSeeActivity`, `canSeeYourFriends`, `visible`
   - output: `{message: 'success'}`   
- `/listings`    [GET]
   - input: `userid` or `city` or (`lat` and `lon`) or `occasionid`
   - output: all listing data as per query
- `/listings`    [POST]
   - input: all listing data
   - output: saved listing data
- `/upload`    [POST]  (multipart-form-data)
   - input: `userid`, `image`(file)
   - output: image URL
- `/upload`    [GET]
   - web page to upload image (for testing purposes)
   - output: a form to upload image 
- `/listings/upload`    [GET]
   - web page to upload image (for testing purposes)
   - output: a form to upload image 
- `/listings/upload`    [POST]  (multipart-form-data)
   - input: `userid`, `image`(file)
   - output: image URL
- `/deletelisting/<userid>`    [DELETE]
   - input: in the url
   - output: `{message: "Deleted listing successfully"}`
- `/deletealllistings`    [DELETE]
   - input: 
   - output: `{message: "all listings deleted"}`
- `/deleteuser/<userid>`    [DELETE]
   - input: in the url
   - output: `{message: "successfully removed user!"}`
- `/deleteallusers`    [DELETE]
   - input: 
   - output: `{message: "all users deleted"}`
- `/getoccasion`    [GET]
   - input: `userid` or `city`
   - output: all listing data as per query
- `/addoccasion`    [POST]
   - input: `name`, `city`
   - output: saved occasion data
- `/occasion/addlisting`    [POST]
   - input: `userid`, `listingid`(array)
   - output: saved occasion data
- `/occasion/upload`    [POST]  (multipart-form-data)
   - input: `userid`, `image`(file)
   - output: image URL
- `/occasion/upload`    [GET]
   - web page to upload image (for testing purposes)
   - output: a form to upload image 
- `/deleteoccasion/<userid>`    [DELETE]
   - input: in the url
   - output: `{message: "successfully removed user!"}`
- `/deletealloccasions`    [DELETE]
   - input: 
   - output: `{message: "all users deleted"}`
- `/addcomment`    [POST]
   - input: `userid`, `listingid`, `rating`, `data`
   - output: saved comment data
- `/getcomment`    [GET]
   - input: `lisgintid` or `commentid`
   - output: all comment or listing data as per query   
- `/deletecomment/<commentid>`    [DELETE]
   - input: in the url
   - output: `{message: "successfully removed comment!"}`
- `/deleteallcomments`    [DELETE]
   - input: 
   - output: `{message: "all comments deleted"}`    